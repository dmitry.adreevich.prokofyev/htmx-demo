#!/bin/bash
# Example build.sh content with error handling and colorized output

# ANSI color codes
RED='\033[0;31m'
GREEN='\033[0;32m'
NO_COLOR='\033[0m'

# Step 0: Go mod tidy
echo "Running go mod tidy..."
if ! go mod tidy; then
    echo -e "${RED}Go mod tidy failed.${NO_COLOR}"
    exit 1
else
    echo -e "${GREEN}Go mod tidy succeeded.${NO_COLOR}"
fi
# Step 1: Compile templ files
echo "Compiling templ files..."
if ! templ generate; then
    echo -e "${RED}Templating generation failed.${NO_COLOR}"
    exit 1
else
    echo -e "${GREEN}Templating generation succeeded.${NO_COLOR}"
fi
# Step 2: Build the Go application
echo "Building Go application..."
if ! go build -o ./tmp/main .; then
    echo -e "${RED}Go build failed.${NO_COLOR}"
    exit 1
else
    echo -e "${GREEN}Go build succeeded.${NO_COLOR}"
fi

# Step 3: Reformat the templ files after successful build
echo "Reformatting templ files..."
if ! templ fmt .; then
    echo -e "${RED}Templ formatting failed.${NO_COLOR}"
    exit 1
else
    echo -e "${GREEN}Templ formatting succeeded.${NO_COLOR}"
fi

echo -e "${GREEN}All steps completed successfully.${NO_COLOR}"
