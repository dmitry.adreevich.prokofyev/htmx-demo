module htmx_test

go 1.20

require (
	github.com/PuerkitoBio/goquery v1.8.1
	github.com/a-h/templ v0.2.476
	github.com/google/uuid v1.6.0
	github.com/gorilla/websocket v1.5.1
	github.com/labstack/echo/v4 v4.11.2
	github.com/lithammer/fuzzysearch v1.1.8
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/labstack/gommon v0.4.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.2 // indirect
	golang.org/x/crypto v0.19.0 // indirect
	golang.org/x/net v0.17.0 // indirect
	golang.org/x/sys v0.17.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	golang.org/x/time v0.5.0 // indirect
)
