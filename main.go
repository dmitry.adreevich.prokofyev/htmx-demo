package main

import (
	"context"
	"encoding/json"
	"github.com/a-h/templ"
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/lithammer/fuzzysearch/fuzzy"
	web "htmx_test/templates"
	"io"
	"log"
	"math/rand"
	"net/http"
	"time"
)

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func main() {
	e := echo.New()
	e.Renderer = &TemplateRenderer{}

	e.GET("/health", func(c echo.Context) error {
		return c.NoContent(http.StatusOK)
	})

	e.GET("/modal", func(c echo.Context) error {
		return c.Render(http.StatusOK, "", web.Modal())

	})

	e.GET("/open-modal", func(c echo.Context) error {
		return c.Render(http.StatusOK, "", web.OpenModal())
	})
	e.GET("/", func(c echo.Context) error {
		return c.Render(http.StatusOK, "", web.Index())
	})
	e.GET("/ag-grid", func(c echo.Context) error {

		return c.Render(http.StatusOK, "", web.AgGrid())
	})

	e.GET("/graph", func(c echo.Context) error {
		return c.Render(http.StatusOK, "", web.GraphWithButton(uuid.New().String(), generateRandomChart()))
	})

	e.GET("/candlestick", func(c echo.Context) error {
		return c.Render(http.StatusOK, "", web.CandleStickGraphWithButton(uuid.New().String(), generateRandomCandleStickChart()))
	})

	e.GET("/long-request", func(c echo.Context) error {

		time.Sleep(3 * time.Second)

		return c.String(http.StatusOK, "Long request done")
	})

	e.GET("/multigraph", func(c echo.Context) error {

		graphNumber := rand.Intn(200) + 1
		graphNumber = 100
		graphs := make([]templ.Component, graphNumber)

		for i := 0; i < graphNumber; i++ {
			graphs[i] = web.CandleStickGraph(uuid.New().String(), generateRandomCandleStickChart())
		}
		return c.Render(http.StatusOK, "", web.MultiGraph(graphs))
	})

	listOfTokesn := []string{
		"BTC",
		"ETH",
		"XRP",
		"ADA",
		"DOT",
		"UNI",
		"LINK",
		"XLM",
		"USDT",
		"DOGE",
		"VET",
		"TRX",
		"XMR",
		"NEO",
		"EOS",
		"XTZ",
		"ATOM",
		"ALGO",
		"ZEC",
		"MKR",
	}

	e.GET("/price-query", func(c echo.Context) error {
		return c.Render(http.StatusOK, "", web.PriceQuery(listOfTokesn))

	})

	e.POST("/price-query", func(c echo.Context) error {
		search := c.FormValue("search")

		result := listOfTokesn
		if search != "" {
			result = fuzzy.FindFold(search, listOfTokesn)
		}
		return c.Render(http.StatusOK, "", web.PriceQueryResults(result))

	})

	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "method=${method}, uri=${uri}, status=${status}, latency=${latency_human}\n",
		Skipper: func(c echo.Context) bool {
			if c.Path() == "/health" {
				return true // Skip logging
			}
			return false // Log other requests
		},
	}))

	e.GET("/ws", func(c echo.Context) error {

		upgrader.CheckOrigin = func(r *http.Request) bool { return true }
		conn, err := upgrader.Upgrade(c.Response(), c.Request(), nil)
		if err != nil {
			return err
		}
		defer conn.Close()

		go func() {

			for {
				car := map[string]interface{}{
					"make":  randomMake(),
					"model": randomModel(),
					"price": rand.Intn(100000),
				}
				msg, err := json.Marshal(car)
				if err != nil {
					log.Println(err)
					break
				}
				if err := conn.WriteMessage(websocket.TextMessage, msg); err != nil {
					log.Println(err)
					break
				}
				time.Sleep(100 * time.Millisecond) // Send a new car every 2 seconds
			}
			log.Println("ws connection closed")
		}()

		for {
			_, _, err := conn.ReadMessage()
			if err != nil {
				if websocket.IsCloseError(err, websocket.CloseNormalClosure) {
					log.Println("Normal closure")
					err := conn.Close()
					if err != nil {
						log.Println(err)
					}
				} else {
					log.Printf("Unexpected closure: %v", err)
				}
				break
			}
		}

		log.Println("ws connection closed")
		return nil
	})

	e.Start(":8080")
	select {}
}
func randomMake() string {
	makes := []string{"Toyota", "Ford", "Porsche", "BMW", "Tesla", "Honda"}
	return makes[rand.Intn(len(makes))]
}

func randomModel() string {
	models := []string{"Celica", "Mondeo", "Boxster", "5 Series", "Model S", "Civic"}
	return models[rand.Intn(len(models))]
}

// TemplateRenderer is a custom html/template renderer for Echo framework

type TemplateRenderer struct {
}

func (t *TemplateRenderer) Render(w io.Writer, _ string, template interface{}, _ echo.Context) error {
	return template.(templ.Component).Render(context.Background(), w)
}

func generateRandomChart() web.ChartOptions {
	var randomInts [8]int
	for i := range randomInts {
		randomInts[i] = rand.Intn(100) // Generates a random integer between 0 and 99
	}
	return web.ChartOptions{
		XAxis: struct {
			Type string   `json:"type"`
			Data []string `json:"data"`
		}{
			Type: "category",
			Data: []string{"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"},
		},
		YAxis: struct {
			Type string `json:"type"`
		}{
			Type: "value",
		},
		Series: []struct {
			Data []int  `json:"data"`
			Type string `json:"type"`
		}{
			{
				Data: randomInts[:],

				Type: "line",
			},
		},
		Animation: false,
	}
}

func generateRandomCandleStickChart() web.CandleStickChartOptions {

	nbpoints := rand.Intn(5000)
	startDate := time.Now().Add(-time.Hour * 24 * 365)

	return web.CandleStickChartOptions{
		XAxis: struct {
			Type string   `json:"type"`
			Data []string `json:"data"`
		}{
			Type: "category",
			Data: generateSequentialStringDates(startDate, nbpoints),
		},
		YAxis: struct {
			Type string `json:"type"`
		}{
			Type: "value",
		},
		DataZoom: web.DataZoom{
			{
				Type:         "inside",
				Start:        98,
				End:          100,
				MinValueSpan: 10,
			},
			{
				Show:         true,
				Type:         "slider",
				Bottom:       60,
				Start:        98,
				End:          100,
				MinValueSpan: 10,
			},
		},
		Series: []struct {
			Data [][]float64 `json:"data"`
			Type string      `json:"type"`
		}{
			{
				Data: generateRandomOHLCVArrays(nbpoints),
				Type: "candlestick",
			},
		},
		Animation: false,
	}
}

func generateSequentialStringDates(startDate time.Time, numberOfDatesToGenerate int) []string {
	dates := make([]string, numberOfDatesToGenerate)
	for i := 0; i < numberOfDatesToGenerate; i++ {
		dates[i] = startDate.AddDate(0, 0, i).Format("2006-01-02")
	}
	return dates
}

func generateRandomOHLCVArrays(nbOfPointsToGenerate int) [][]float64 {
	rand.Seed(time.Now().UnixNano()) // Ensure different results on each run

	ohlc := make([][]float64, nbOfPointsToGenerate)
	for i := 0; i < nbOfPointsToGenerate; i++ {
		open := rand.Float64() * 1000
		cls := rand.Float64() * 1000
		high := max(open, cls) + rand.Float64()*10 // Ensure high is above open and cls
		low := min(open, cls) - rand.Float64()*10  // Ensure low is below open and cls
		volume := rand.Float64()*1000 + 100        // Random volume, ensuring some base volume

		// Correct any negative low values
		if low < 0 {
			low = min(open, cls) * rand.Float64()
		}

		ohlc[i] = []float64{cls, open, low, high, volume}
	}

	return ohlc
}

// Helper functions to find min and max of two floats
func min(a, b float64) float64 {
	if a < b {
		return a
	}
	return b
}

func max(a, b float64) float64 {
	if a > b {
		return a
	}
	return b
}
