package web

import (
	"context"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"io"
	"testing"
)

func Test_chart_value_injection(t *testing.T) {

	data := ChartOptions{
		ChartType: "line",
		Series: []struct {
			Name string    `json:"name"`
			Data []float64 `json:"data"`
		}{
			{
				Name: "sales",
				Data: []float64{30, 40, 35, 50, 49, 60, 70, 91, 125},
			},
		},
		Xaxis: struct {
			Categories []string `json:"categories"`
		}{
			Categories: []string{"1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999"},
		},
	}

	r, w := io.Pipe()

	go func() {
		err := Graph(data).Render(context.Background(), w)
		if err != nil {
			t.Fatal(err)
		}
		w.Close()
	}()

	a, err := goquery.NewDocumentFromReader(r)
	if err != nil {
		t.Fatal(err)
	}

	fmt.Print(a.Html())

}
